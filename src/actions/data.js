import generateData from "./entry/generateData";

const context = require.context("./../assets/entries/", false, /\.md$/);
const relevant = context.keys().filter((e) => e.startsWith("."));
const content = relevant.map(context);
const entries = content.map((e, i) => ({
  ...e,
  timestamp: new Date(relevant[i].slice(2, -3) * 1000),
}));

const getData = generateData(entries);

export { getData };
