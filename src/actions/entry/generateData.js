// https://gist.github.com/blixt/f17b47c62508be59987b
const LCG = (s) => () => (s = (Math.imul(48271, s) >>> 0) % 2147483647);
const generator = LCG(88888888);

const maxSecondsIncrement = 600000;
const startTime = new Date(2000, 3, 19);
const endTime = new Date(2008, 4, 1); // the Collapse
const timeIncrement = (time) => () =>
  new Date((time += (generator() % maxSecondsIncrement) + 1) * 1000);
const getTime = timeIncrement(startTime.getTime() / 1000);

const pid = generator();

const generateHeader = () => ({
  // IP
  "0100 .... = Version": 4,
  ".... 0101 = Header Length": "20 bytes (5)",
  "Total Length": 1290,
  Identification: generator().toString(16),
  "010. .... = Flags": "0x2, Don't fragment",
  "...0 0000 0000 0000 = Fragment Offset": "0",
  "Time to Live": 120,
  Protocol: "TCP (6)",
  "Header Checksum": generator().toString(16),

  // TCP
  "Source Port": "443",
  "Destination Port": generator() % 65535,
  "Sequence Number": generator() % 10000,
  "Acknowledgement Number": generator() % 1000,
  Flags: "0x010 (ACK)",
  Window: generator() % 65535,
});

const generateEntry = (time) => {
  const message = () => ({
    origin: generator() % 10 < 9 ? `user:${generator()}` : `bot:${generator()}`,
    target: generator() % 10 < 9 ? `user:${generator()}` : `bot:${generator()}`,
    type: "MESSAGE",
  });

  const post = () => ({
    origin: generator() % 10 < 9 ? `user:${generator()}` : `bot:${generator()}`,
    target: `thread:${generator()}`,
    type: "POST",
  });

  const ban = () => ({
    origin: `moderator:${generator()}`,
    target: `user:${generator()}`,
    type: "BAN",
  });

  const getType = () => {
    const probability = generator() % 100;
    if (probability < 70) {
      return post();
    } else if (probability < 95) {
      return message();
    } else {
      return ban();
    }
  };

  return {
    time: time.toUTCString(),
    ...getType(),
    info: "NOMINAL",
    data: generateHeader(),
  };
};

const generateEntriesUntil = (timestamp) => {
  const generateTimestamps = function* () {
    let stamp = getTime();
    while (stamp < timestamp && stamp < endTime) {
      yield stamp;
      stamp = getTime();
    }
  };

  return [...generateTimestamps()].map(generateEntry);
};

const generateData = function* (genuineEntries) {
  for (let genuineEntry of genuineEntries) {
    let entries = [
      ...generateEntriesUntil(genuineEntry.timestamp),
      {
        time: genuineEntry.timestamp.toUTCString(),
        origin: `bot:${pid}`,
        target: `bot:${pid}`,
        type: "BOT",
        info: "FLAGGED",
        data: {
          ...generateHeader(),
          "???": genuineEntry.default,
        },
      },
    ];
    yield entries;
  }
};

export default generateData;
