import React, { useState } from "react";

import {
  EntryContainer,
  EntryElementsContainer,
  EntryElementContainer,
  EntryDataContainer,
} from "assets/styles/Entry.styles.jsx";

import constants from "assets/constants.js";

const Entry = ({ data }) => {
  const [opened, setOpened] = useState(false);

  return (
    <EntryContainer onClick={() => setOpened((prev) => !prev)}>
      <EntryElementsContainer>
        {constants.headers.map((header) => (
          <EntryElementContainer key={header}>
            {data[header]}
          </EntryElementContainer>
        ))}
      </EntryElementsContainer>
      {opened && (
        <EntryDataContainer>
          {Object.entries(data.data).map(([key, value]) => (
            <div key={key}>
              {key}: {value}
            </div>
          ))}
        </EntryDataContainer>
      )}
    </EntryContainer>
  );
};

export default Entry;
