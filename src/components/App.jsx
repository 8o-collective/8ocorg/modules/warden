import React, { useState, useEffect } from "react";

import {
  AppContainer,
  AppHeaderContainer,
  AppHeaderElementContainer,
} from "assets/styles/App.styles.jsx";

import Entry from "components/Entry.jsx";

import constants from "assets/constants.js";

import { getData } from "actions/data.js";

const pixelBuffer = 3000; // 3000 is fast enough to outpace the scroll hitting the bottom before we can process new data

const AppHeader = () => (
  <AppHeaderContainer>
    {constants.headers.map((e) => (
      <AppHeaderElementContainer key={e}>{e}</AppHeaderElementContainer>
    ))}
  </AppHeaderContainer>
);

const App = () => {
  const [data, setData] = useState(getData.next().value);

  const updateData = () =>
    setData((prev) => [...prev, ...(getData.next().value ?? [])]);

  const handleScroll = () => {
    const scrollTop =
      (document.documentElement && document.documentElement.scrollTop) ||
      document.body.scrollTop;

    const scrollHeight =
      (document.documentElement && document.documentElement.scrollHeight) ||
      document.body.scrollHeight;

    if (scrollTop + window.innerHeight + pixelBuffer >= scrollHeight)
      updateData();
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return (
    <AppContainer>
      <AppHeader />
      {data.map((e) => (
        <Entry key={e.time} data={e} />
      ))}
    </AppContainer>
  );
};

export default App;
