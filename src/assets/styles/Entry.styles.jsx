import styled from "styled-components";

const EntryContainer = styled.div`
  cursor: pointer;

  box-sizing: border-box;

  &:hover {
    border: solid red 1px;
  }
`;

const EntryElementsContainer = styled.div`
  margin: 0px;
  width: 100%;
  left: 0px;

  display: flex;
  justify-content: space-evenly;

  font-family: IBM3270;
  color: white;
`;

const EntryElementContainer = styled.div`
  width: 100%;
  padding: 0.5vw;
  background-color: black;

  //   border: solid red 1px;
  //   border-right: none;
`;

const EntryDataContainer = styled.div`
  display: block;
  padding: 1vw;
  width: 50%;
`;

export {
  EntryContainer,
  EntryElementsContainer,
  EntryElementContainer,
  EntryDataContainer,
};
